﻿using UnityEngine;
using System.Collections;

public class ObjectInGrid : MonoBehaviour {
	
	public GridCell currentCell;

	protected virtual void Start() {
		OccupyCell();
	}
	
	public virtual void OccupyCell() {
		GridCell onTopOf = Grid.grid.FindCell(transform.position);
		OccupyCell(onTopOf);
	}
	
	public virtual void OccupyCell(GridCell _newCell) {	
		if(_newCell.Occupy(this)) {
			currentCell = _newCell;
		}
	}
	
	public virtual void FreeCurrentCell() {
		if(currentCell != null) {
			currentCell.FreeCell();
			currentCell = null;
		}
	}
}
