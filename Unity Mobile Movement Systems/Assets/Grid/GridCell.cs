﻿using UnityEngine;
using System.Collections;

public class GridCell : MonoBehaviour {
	
	public ObjectInGrid occupant;
	
	public virtual bool Occupy(ObjectInGrid _newOccupant) {
		if(occupant == null) {
			occupant = _newOccupant;
			return true;
		}
		else {
			return false;
		}
	}
	
	public virtual void FreeCell() {
		occupant = null;
	}
	
	public virtual bool IsFree() {
		return occupant == null;
	}
}
