﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Grid : MonoBehaviour
{
    public GridCell cellPrefab;
	
	public static Grid grid;
	public List<GridCell> gridCells;
	
	public float[] extremes = new float[4];
	private enum Direction {
		Up,
		Right,
		Down,
		Left
	}
	
	void Start() {
		grid = this;
		if(gridCells.Count <= 0) {
			SetupGrid();
		}
	}
	
	public void SetupGrid() {
		gridCells = new List<GridCell>();
		GridCell[] tempGrid = FindObjectsOfType<GridCell>();
		
		extremes = new float[] { tempGrid[0].transform.position.y,
								 tempGrid[0].transform.position.x,
								 tempGrid[0].transform.position.y,
								 tempGrid[0].transform.position.x };
		
		foreach(GridCell gc in tempGrid) {
		    gridCells.Add(gc);

            if (gc.transform.position.z > extremes[(int)Direction.Up]) {
				extremes[(int)Direction.Up] = gc.transform.position.z;
			}
			if(gc.transform.position.z < extremes[(int)Direction.Down]) {
				extremes[(int)Direction.Down] = gc.transform.position.z;
			}
			if(gc.transform.position.x > extremes[(int)Direction.Right]) {
				extremes[(int)Direction.Right] = gc.transform.position.x;
			}
			if(gc.transform.position.x < extremes[(int)Direction.Left]) {
				extremes[(int)Direction.Left] = gc.transform.position.x;
			}
		}
	}

    public GridCell FindCell(Vector3 position) {
		return FindCell(new Vector2(position.x, position.z));
	}
	
	public GridCell FindCell(Vector2 coordinates) {
	    int x = Mathf.RoundToInt(coordinates.x);
	    int y = Mathf.RoundToInt(coordinates.y);
	    coordinates = new Vector2(x, y);
		if(OutOfBounds(coordinates)) {
			//Debug.Log("That cell is far far away... " + coordinates);
			return null;
		}

	    return gridCells.FirstOrDefault(gc => Mathf.RoundToInt(gc.transform.position.x) == x &&
	                                          Mathf.RoundToInt(gc.transform.position.z) == y);
	    //Debug.Log("Tried to find cell at " + coordinates + " but it failed.");
	}
	
	public bool OutOfBounds(Vector3 position) {
		return OutOfBounds(new Vector2(position.x, position.z));
	}
	
	public bool OutOfBounds(Vector2 coordinates) {
		return !((extremes[(int)Direction.Left] <= coordinates.x && coordinates.x <= extremes[(int)Direction.Right]) && (extremes[(int)Direction.Down] <= coordinates.y && coordinates.y <= extremes[(int)Direction.Up]));
	}
	
	public GridCell GetRandomEmptyCell() {
		List<GridCell> emptyCells = new List<GridCell>();
		foreach(GridCell gc in gridCells) {
			if(gc.IsFree()) {
				emptyCells.Add(gc);
			}
		}
		
		if(emptyCells.Count == 0) {
			return null;
		}
		else {
			return emptyCells[Random.Range(0, emptyCells.Count)];
		}
	}
}
