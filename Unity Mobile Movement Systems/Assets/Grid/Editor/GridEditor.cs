﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using GridbasedMovementPointNClick;
using UnityEditor.Graphs;

public class GridEditor : EditorWindow
{
    private int gridWidth = 1;
    private int gridHeight = 1;

    // Add menu item named "Grid Editor" to the Window menu
    [MenuItem("Window/Grid Editor")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(GridEditor));
	}

	void OnGUI()
	{
		GUILayout.Label ("Grid Tools", EditorStyles.boldLabel);

	    gridWidth = EditorGUILayout.IntField("Grid Width", gridWidth);
	    gridHeight = EditorGUILayout.IntField("Grid Height", gridHeight);

	    if(GUILayout.Button("Create Grid"))
	    {
	        CreateGrid();
	    }

        if (GUILayout.Button("Setup Grid"))
		{
		    SetupGrid();
		}
		
		if(GUILayout.Button("Remove overlaps"))
		{
		    RemoveOverlaps();
		}

        if (GUILayout.Button("Set Neighbours"))
        {
            SetNeighbours();
        }
	}

    private void CreateGrid()
    {
        Grid grid = FindObjectOfType<Grid>();
        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                GridCell newCell = PrefabUtility.InstantiatePrefab(grid.cellPrefab) as GridCell;
                if(newCell != null) {
                    newCell.transform.parent = grid.transform;
                    newCell.transform.localPosition = new Vector3(i, 0, j);
                }
                else
                {
                    Debug.LogError("New cell created but not handled");
                    return;
                }
            }
        }
        if(gridWidth > 0 && gridHeight > 0)
        {
            SetupGrid();
            if (grid.cellPrefab is GridCellWithNeighbours)
            {
                SetNeighbours();
            }
        }
        Debug.Log("Created new grid (" + gridWidth + "," + gridHeight + ")");
    }

    private static void SetupGrid()
    {
        Grid grid = FindObjectOfType<Grid>();
        if (grid != null)
        {
            grid.SetupGrid();
        }
    }

    private static void RemoveOverlaps()
    {
        Grid grid = FindObjectOfType<Grid>();
        if (grid == null)
        {
            return;
        }
        List<Vector2> cellPositions = new List<Vector2>();
        foreach(GridCell gc in grid.gridCells)
        {
            Vector2 tempPos = new Vector2(gc.transform.position.x, gc.transform.position.z);
            if(cellPositions.Contains(tempPos))
            {
                Debug.LogWarning("Removed cell at " + gc.transform.position);
                DestroyImmediate(gc.gameObject);
            }
            else
            {
                cellPositions.Add(tempPos);
            }
        }
    }

    private static void SetNeighbours()
    {
        Grid grid = FindObjectOfType<Grid>();
        if (grid == null)
        {
            return;
        }
        
        foreach (var gc in grid.gridCells.OfType<GridCellWithNeighbours>())
        {
            float x = gc.transform.position.x,
                  z = gc.transform.position.z;

            GridCellWithNeighbours cell = (GridCellWithNeighbours)gc;
            GridCellWithNeighbours rightCell = grid.FindCell(new Vector2(x + 1, z)) as GridCellWithNeighbours;
            GridCellWithNeighbours downCell = grid.FindCell(new Vector2(x, z - 1)) as GridCellWithNeighbours;

            cell.eastNeighbour = rightCell;
            if (rightCell != null)
            {
                rightCell.westNeighbour = cell;
            }
            cell.southNeighbour = downCell;
            if (downCell != null)
            {
                downCell.northNeighbour = cell;
            }
        }
    }
}
