﻿using UnityEngine;
using System.Collections;

public class Moveable : ObjectInGrid {
    [SerializeField]
	protected bool isMoving = false;
	public float moveSpeed = 5;

    public bool inMovement => isMoving;

	public virtual void MoveInGrid(GridCell _newCell) {
		if(_newCell != null) {
			StopAllCoroutines();
			StartCoroutine(MoveTowards(_newCell));
		}
    }

    public void MoveToPosition(Vector3 position)
    {
        transform.position = position.WithY(transform.position.y);
    }

    protected virtual IEnumerator MoveTowards(GridCell _newCell) {
		isMoving = true;
		
		FreeCurrentCell();
	    OccupyCell(_newCell);

        Vector3 newPosition = _newCell.transform.position;
        Vector3 moveDir = newPosition - transform.position;
		moveDir.y = 0;

        transform.forward = moveDir;
		
		while(moveDir.magnitude > moveSpeed * Time.deltaTime) {
			transform.Translate(moveDir.normalized * moveSpeed * Time.deltaTime, Space.World);
			yield return new WaitForEndOfFrame();
			moveDir = newPosition - transform.position;
			moveDir.y = 0;
		}
		
		MoveToPosition(newPosition);
		isMoving = false;
		
		ArriveAtCell(_newCell);
	}

    protected virtual void ArriveAtCell(GridCell cell)
    {
    }
	
	protected bool CheckMovement(GridCell _cell) {
		return _cell.IsFree();
	}
}
