﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FreeMovementPointNClick
{
    public interface IClickableObject
    {
        Transform transform { get; }

        GameObject gameObject { get; }

        T GetComponent<T>();

        float size { get; }
    } 
}
