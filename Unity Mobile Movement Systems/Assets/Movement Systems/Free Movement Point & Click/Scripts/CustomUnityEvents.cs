﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace FreeMovementPointNClick
{
    [Serializable]
    public class PositionUnityEvent : UnityEvent<Vector3>
    {
        
    }

    [Serializable]
    public class ClickableTargetUnityEvent : UnityEvent<IClickableObject>
    {

    }
}
