﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FreeMovementPointNClick
{
    public abstract class Interactable : MonoBehaviour, IClickableObject
    {
        [SerializeField]
        private float _size = 1;

        public float size => _size;

        public abstract void Interact(MonoBehaviour behaviour);
    } 
}
