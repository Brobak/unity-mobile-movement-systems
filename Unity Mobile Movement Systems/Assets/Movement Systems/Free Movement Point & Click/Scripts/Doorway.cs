﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FreeMovementPointNClick
{
    public class Doorway : Interactable
    {
        public Vector3 targetPosition;

        public override void Interact(MonoBehaviour behaviour)
        {
            NavMeshAgent agent = behaviour.GetComponent<NavMeshAgent>();
            if(agent != null)
            {
                agent.Warp(targetPosition);
            }
            else
            {
                behaviour.transform.position = targetPosition;
            }
        }
    } 
}
