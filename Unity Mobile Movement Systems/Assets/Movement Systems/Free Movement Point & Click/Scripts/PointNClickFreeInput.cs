﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace FreeMovementPointNClick
{
    public class PointNClickFreeInput : PointNClickInput<IClickableObject>
    {
        [SerializeField]
        private PositionUnityEvent onMoveClick;
        [SerializeField]
        private ClickableTargetUnityEvent onTargetClick;

        protected override void OnMoveClick(RaycastHit hit)
        {
            onMoveClick.Invoke(hit.point);
        }

        protected override void OnTargetClick(IClickableObject target)
        {
            onTargetClick.Invoke(target);
        }
    } 
}
