﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FreeMovementPointNClick
{
    public class TalkingVillager : Interactable
    {
        public Text fieldForTalk;
        public string dialogue = "Hello";

        private bool isTalking = false;

        public override void Interact(MonoBehaviour behaviour)
        {
            if (isTalking)
            {
                fieldForTalk.text = "";
                isTalking = false;
            }
            else
            {
                fieldForTalk.text = dialogue;
                isTalking = true;
            }
        }
    } 
}
