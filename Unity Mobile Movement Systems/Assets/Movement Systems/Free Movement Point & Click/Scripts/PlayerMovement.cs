﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FreeMovementPointNClick
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private NavMeshAgent agent;

        public NavMeshAgent navAgent => agent;

        private Interactable targetInteractable;
        private float playerSize = 0.5f;

        public void MoveTo(Vector3 position)
        {
            targetInteractable = null;
            navAgent.SetDestination(position);
        }

        public void GoToAndInteract(IClickableObject target)
        {
            Vector3 spotNearTarget = target.transform.position;
            spotNearTarget -= (spotNearTarget - transform.position).normalized * (target.size + playerSize);
            navAgent.SetDestination(spotNearTarget);
            targetInteractable = target as Interactable;
            if(targetInteractable != null)
            {
                StartCoroutine(InteractOnArrivalCoroutine(targetInteractable));
            }
        }

        private IEnumerator InteractOnArrivalCoroutine(Interactable interactable)
        {
            while(agent.pathPending || agent.remainingDistance > interactable.size + playerSize)
            {
                yield return new WaitForEndOfFrame();
                if(interactable != targetInteractable)
                {
                    yield break;
                }
            }
            interactable.Interact(this);
        }

        private void Reset()
        {
            agent = GetComponent<NavMeshAgent>();
        }
    } 
}
