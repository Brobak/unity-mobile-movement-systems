﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Virtual4DPad : MonoBehaviour
{
    public KeyCode[] upKeyBinding = {KeyCode.W, KeyCode.UpArrow}, 
                     rightKeyBinding = {KeyCode.D, KeyCode.RightArrow}, 
                     downKeyBinding = {KeyCode.S, KeyCode.DownArrow}, 
                     leftKeyBinding = {KeyCode.A, KeyCode.LeftArrow};
    public UnityEvent onUp, onRight, onDown, onLeft;

    private bool upPressed, rightPressed, downPressed, leftPressed;

#if !UNITY_ANDROID && !UNITY_IOS
    private void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
#endif

    private void Update()
    {
        if(upKeyBinding.Any(Input.GetKey))
        {
            UpButtonPressed();
        }
        if(rightKeyBinding.Any(Input.GetKey))
        {
            RightButtonPressed();
        }
        if(downKeyBinding.Any(Input.GetKey))
        {
            DownButtonPressed();
        }
        if (leftKeyBinding.Any(Input.GetKey))
        {
            LeftButtonPressed();
        }
    }

    private void LateUpdate()
    {
        upPressed = false;
        rightPressed = false;
        downPressed = false;
        leftPressed = false;
    }

    public void UpButtonPressed()
    {
        if(upPressed)
        {
            return;
        }
        upPressed = true;

        onUp.Invoke();
    }

    public void RightButtonPressed()
    {
        if (rightPressed)
        {
            return;
        }
        rightPressed = true;

        onRight.Invoke();
    }

    public void DownButtonPressed()
    {
        if (downPressed)
        {
            return;
        }
        downPressed = true;

        onDown.Invoke();
    }

    public void LeftButtonPressed()
    {
        if (leftPressed)
        {
            return;
        }
        leftPressed = true;

        onLeft.Invoke();
    }
}
