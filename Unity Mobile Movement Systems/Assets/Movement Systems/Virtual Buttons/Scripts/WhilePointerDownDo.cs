﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class WhilePointerDownDo : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    private UnityEvent doWhileDown;
    [SerializeField]
    private UnityEvent onPointerDown;
    [SerializeField]
    private UnityEvent onPointerUp;

    private bool isDown;

    private void Update()
    {
        if(isDown)
        {
            doWhileDown.Invoke();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
        onPointerDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
        onPointerUp.Invoke();
    }
}
