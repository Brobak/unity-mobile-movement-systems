﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRadialPositionProvider
{
    float radius { get; }

    Vector2 centerToCursorVector { get; }
}
