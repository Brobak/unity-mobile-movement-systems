﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class VirtualActionButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public KeyCode[] keyBinding = {KeyCode.Space};

    public UnityEvent onButtonDown;
    public UnityEvent onButtonPressed;
    public UnityEvent onButtonUp;

    private bool buttonPressed = false;

#if !UNITY_ANDROID && !UNITY_IOS
    private void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
#endif

    private void Update()
    {
        if(buttonPressed || keyBinding.Any(Input.GetKey))
        {
            onButtonPressed.Invoke();
        }
        if(keyBinding.Any(Input.GetKeyDown))
        {
            onButtonDown.Invoke();
        }
        if(keyBinding.Any(Input.GetKeyUp))
        {
            onButtonUp.Invoke();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonPressed = true;
        onButtonDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonPressed = false;
        onButtonUp.Invoke();
    }
}
