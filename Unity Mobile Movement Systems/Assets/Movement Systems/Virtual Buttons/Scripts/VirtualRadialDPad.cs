﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class VirtualRadialDPad : MonoBehaviour
{
    public RadialPositionEvent onDPad;
    public RadialPositionEvent onDPadDown;
    public RadialPositionEvent onDPadUp;

    [SerializeField]
    private CircleRaycastFilter positionProvider;

    private bool dpadPressed = false;

    #region Keyboard Input
    public string horizontalInputAxis = "Horizontal",
                  verticalInputAxis = "Vertical";
    private IRadialPositionProvider keyboardPositionProvider;
    private bool isDown;

    private class KeyboardRadialPositionProvider : IRadialPositionProvider
    {
        private readonly string horizontalInputAxis;
        private readonly string verticalInputAxis;

        public float radius => 1;

        public Vector2 centerToCursorVector
        {
            get
            {
                float x = Input.GetAxis(horizontalInputAxis), y = Input.GetAxis(verticalInputAxis);
                Vector2 result = new Vector2(x, y);
                float magnitude = new[]{x, y}.Select(Mathf.Abs).Max();
                return result.normalized * magnitude;
            }
        }

        public KeyboardRadialPositionProvider(string horizontalInputAxis, string verticalInputAxis)
        {
            this.horizontalInputAxis = horizontalInputAxis;
            this.verticalInputAxis = verticalInputAxis;
        }
    }

    private void Awake()
    {
        keyboardPositionProvider = new KeyboardRadialPositionProvider(horizontalInputAxis, verticalInputAxis);
    }

    private void Update()
    {
        if(keyboardPositionProvider.centerToCursorVector != Vector2.zero)
        {
            if (!isDown)
            {
                isDown = true;
                onDPadDown.Invoke(Vector2.zero);
            }

            DPadPressed(keyboardPositionProvider);
        }
        else if (isDown)
        {
            isDown = false;
            onDPadUp.Invoke(Vector2.zero);
        }
    }
    #endregion

#if !UNITY_ANDROID && !UNITY_IOS
    private void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
#endif

    private void LateUpdate()
    {
        dpadPressed = false;
    }

    public void DPadPressed()
    {
        DPadPressed(positionProvider);
    }

    private void DPadPressed(IRadialPositionProvider pp)
    {
        if (dpadPressed)
        {
            return;
        }
        dpadPressed = true;

        Vector2 relativeRadialPosition = pp.centerToCursorVector / pp.radius;

        onDPad.Invoke(relativeRadialPosition);
    }

    public void DPadDown()
    {
        Vector2 relativeRadialPosition = positionProvider.centerToCursorVector / positionProvider.radius;

        onDPadDown.Invoke(relativeRadialPosition);
    }

    public void DPadUp()
    {
        Vector2 relativeRadialPosition = positionProvider.centerToCursorVector / positionProvider.radius;

        onDPadUp.Invoke(relativeRadialPosition);
    }

    private void Reset()
    {
        positionProvider = GetComponentInChildren<CircleRaycastFilter>();
    }
}
