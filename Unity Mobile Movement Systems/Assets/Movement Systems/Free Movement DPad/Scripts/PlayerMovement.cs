﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FreeMovementDPad
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
        public float moveSpeed = 5;

        [SerializeField]
        private PlayerAnimation aniHandler;
        [SerializeField]
        private Rigidbody playerRigidbody;

        public void Move(Vector2 direction)
        {
            if (!gameObject.activeInHierarchy || !enabled)
            {
                return;
            }

            Vector3 dir3D = new Vector3(direction.x, 0, direction.y);
            transform.forward = dir3D;
            playerRigidbody.velocity = dir3D * moveSpeed;

            aniHandler.Run(direction.magnitude);
        }

        public void StopMoving()
        {
            playerRigidbody.velocity = Vector3.zero;
            aniHandler.Idle();
        }

        private void Reset()
        {
            aniHandler = GetComponentInChildren<PlayerAnimation>();
            playerRigidbody = GetComponent<Rigidbody>();
        }
    }
}
