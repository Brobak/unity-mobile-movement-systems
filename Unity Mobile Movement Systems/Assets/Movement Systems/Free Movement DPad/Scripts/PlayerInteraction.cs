﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FreeMovementDPad
{
    public class PlayerInteraction : MonoBehaviour
    {
        private List<IInteractable> currentInteractables = new List<IInteractable>();

        private void OnTriggerEnter(Collider other)
        {
            IInteractable[] hits = other.GetComponents<IInteractable>();
            currentInteractables.AddRange(hits);
        }

        private void OnTriggerExit(Collider other)
        {
            IInteractable[] hits = other.GetComponents<IInteractable>();
            foreach (IInteractable hit in hits.ToList())
            {
                currentInteractables.Remove(hit);
            }
        }

        public void InteractNow()
        {
            if (!currentInteractables.Any())
            {
                return;
            }
            IInteractable mostInFrontInteractable = currentInteractables
                .OrderBy(i => Vector3.Angle(transform.forward, Vector3.ProjectOnPlane(i.transform.position - transform.position, Vector3.up)))
                .First();
            InteractWith(mostInFrontInteractable);
        }

        public void InteractWith(IInteractable interactable)
        {
            interactable.Interact();
        }
    }
}
