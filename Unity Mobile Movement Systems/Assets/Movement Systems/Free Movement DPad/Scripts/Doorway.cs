﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FreeMovementDPad
{
    public class Doorway : MonoBehaviour
    {
        public Vector3 targetPosition;

        [SerializeField]
        private float enteringAngle = 45;

        private Rigidbody playerRigidbody;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                playerRigidbody = other.GetComponent<Rigidbody>();
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if(other.CompareTag("Player") && IsGoingThroughDoor())
            {
                TransportThroughDoorway(other.transform);
            }
        }

        private bool IsGoingThroughDoor()
        {
            if(playerRigidbody.velocity == Vector3.zero)
            {
                return false;
            }
            return Vector3.Angle(playerRigidbody.velocity, -transform.right) < enteringAngle;
        }

        private void TransportThroughDoorway(Transform user)
        {
            user.position = targetPosition;
        }
    }
}

