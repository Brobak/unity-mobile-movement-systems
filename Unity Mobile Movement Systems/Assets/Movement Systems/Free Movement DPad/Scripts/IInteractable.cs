﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FreeMovementDPad
{
    public interface IInteractable
    {
        Transform transform { get; }

        void Interact();
    }
}
