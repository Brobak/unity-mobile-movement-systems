using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridbasedMovementDPad
{
    public class PlayerMovement : Moveable
    {
        [SerializeField]
        private PlayerAnimation aniHandler;

        public void TryMoveUp()
        {
            TryMove(Vector3.forward);
        }

        public void TryMoveRight()
        {
            TryMove(Vector3.right);
        }

        public void TryMoveDown()
        {
            TryMove(Vector3.back);
        }

        public void TryMoveLeft()
        {
            TryMove(Vector3.left);
        }

        private void TryMove(Vector3 direction) { 
            if(isMoving)
            {
                return;
            }

            GridCell tryCell = Grid.grid.FindCell(transform.position + direction);
            if(tryCell != null && tryCell.IsFree())
            {
                aniHandler.Run(1);
                MoveInGrid(tryCell);
            }
        }

        protected override IEnumerator MoveTowards(GridCell _newCell)
        {
            IEnumerator standard = base.MoveTowards(_newCell);

            while(standard.MoveNext())
            {
                yield return standard.Current;
            }

            yield return new WaitForEndOfFrame();

            if(!isMoving)
            {
                aniHandler.Idle();
            }
        }
    }
}
