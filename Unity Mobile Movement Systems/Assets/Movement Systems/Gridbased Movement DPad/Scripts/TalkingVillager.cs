﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GridbasedMovementDPad
{
    public class TalkingVillager : Moveable, IInteractable
    {
        public Text fieldForTalk;
        public string dialogue = "Hello";

        private bool isTalking = false;

        public void Interact()
        {
            if (isTalking)
            {
                fieldForTalk.text = "";
                isTalking = false;
            }
            else
            {
                fieldForTalk.text = dialogue;
                isTalking = true;
            }
        }
    }
}
