﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridbasedMovementDPad
{
    public class DoorwayCell : GridCell
    {
        public GridCell landingTarget;
        public GridCell goToTarget;

        private Vector3 landingPosition => landingTarget.transform.position;

        public override bool Occupy(ObjectInGrid _newOccupant)
        {
            if (!base.Occupy(_newOccupant))
            {
                return false;
            }
            Moveable moveableOccupant = _newOccupant as Moveable;
            if(moveableOccupant == null)
            {
                return true;
            }
            FreeCell();
            moveableOccupant.MoveToPosition(landingPosition);
            moveableOccupant.MoveInGrid(goToTarget);

            // Occupant should not occupy this cell since it has already moved on to goToTarget
            return false;
        }
    } 
}
