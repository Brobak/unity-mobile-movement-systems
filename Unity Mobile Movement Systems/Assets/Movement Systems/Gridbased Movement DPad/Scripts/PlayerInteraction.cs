﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridbasedMovementDPad
{
    public class PlayerInteraction : MonoBehaviour
    {
        [SerializeField]
        private PlayerMovement movement;

        private void Reset()
        {
            movement = GetComponent<PlayerMovement>();
        }

        public void InteractWithFront()
        {
            if(movement.inMovement)
            {
                return;
            }
            GridCell interactingCell = Grid.grid.FindCell(transform.position + transform.forward);

            IInteractable interactable = null;
            if (interactingCell != null)
            {
                interactable = interactingCell.occupant as IInteractable;
            }
            
            interactable?.Interact();
        }
    } 
}
