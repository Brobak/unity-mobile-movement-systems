﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridbasedMovementDPad
{
    public interface IInteractable
    {
        void Interact();
    } 
}
