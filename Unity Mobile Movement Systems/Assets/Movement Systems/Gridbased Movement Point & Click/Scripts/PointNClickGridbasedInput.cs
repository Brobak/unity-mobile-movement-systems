﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace GridbasedMovementPointNClick
{
    public class PointNClickGridbasedInput : PointNClickInput<IClickableObject>
    {
        [SerializeField]
        private GridCellUnityEvent onMoveClick;
        [SerializeField]
        private ClickableTargetUnityEvent onTargetClick;

        protected override void OnMoveClick(RaycastHit hit)
        {
            onMoveClick.Invoke(hit.collider.GetComponent<GridCell>());
        }

        protected override void OnTargetClick(IClickableObject target)
        {
            onTargetClick.Invoke(target);
        }
    } 
}
