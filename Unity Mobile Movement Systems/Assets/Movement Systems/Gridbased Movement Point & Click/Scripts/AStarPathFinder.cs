﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public class AStarPathFinder
    {
        private readonly Vector3 center;
        private readonly GridCell from;
        private readonly IEnumerable<GridCell> to;
        private readonly bool allowDiagonals;

        private readonly PriorityList<GridCellWithNeighbours> frontier = new PriorityList<GridCellWithNeighbours>();
        private readonly List<GridCellWithNeighbours> closedList = new List<GridCellWithNeighbours>();
        private readonly Dictionary<GridCell, GridCell> backtrack = new Dictionary<GridCell, GridCell>();

        private readonly Dictionary<GridCell, float> g = new Dictionary<GridCell, float>();

        public AStarPathFinder(GridCell from, GridCell to, bool allowDiagonals) : this(from, new[] { to }, allowDiagonals)
        { }

        public AStarPathFinder(GridCell from, IEnumerable<GridCell> to, bool allowDiagonals)
        {
            List<GridCell> toList = to.ToList();
            center = new Vector3(
                toList.Select(c => c.transform.position.x).Average(),
                0,
                toList.Select(c => c.transform.position.z).Average());
            this.from = from;
            this.to = toList;
            this.allowDiagonals = allowDiagonals;
        }

        public List<GridCell> FindPath()
        {
            GridCellWithNeighbours cell = from as GridCellWithNeighbours;
            frontier.Add(cell, 0);
            g.Add(cell, 0);

            while (frontier.Any())
            {
                GridCellWithNeighbours current = frontier.Pop();
                closedList.Add(current);

                if (to.Contains(current))
                {
                    return BacktrackPath(backtrack, current);
                }

                ExpandFrontier(current);
            }

            return null;
        }

        private void ExpandFrontier(GridCellWithNeighbours current)
        {
            List<GridCellWithNeighbours> adjacents = GetValidAdjacentCells(current);

            foreach (GridCellWithNeighbours adjacent in adjacents)
            {
                if (closedList.Contains(adjacent))
                {
                    continue;
                }

                float newG = g[current] + (current.adjacents().Contains(adjacent) ? 1 : 1.4f);

                bool contained = false;
                if (frontier.Contains(adjacent))
                {
                    if (newG >= g[adjacent])
                    {
                        continue;
                    }
                    contained = true;
                }

                float newF = newG + h(adjacent);

                if (backtrack.ContainsKey(adjacent))
                {
                    backtrack[adjacent] = current;
                }
                else
                {
                    backtrack.Add(adjacent, current); 
                }

                if (g.ContainsKey(adjacent))
                {
                    g[adjacent] = newG;
                }
                else
                {
                    g.Add(adjacent, newG);
                }

                if (contained)
                {
                    frontier.UpdatePriority(adjacent, newF);
                }
                else
                {
                    frontier.Add(adjacent, newF);
                }
            }
        }

        private List<GridCellWithNeighbours> GetValidAdjacentCells(GridCellWithNeighbours current)
        {
            List<GridCellWithNeighbours> adjacents = new List<GridCellWithNeighbours>();
            bool leftOpen = false, rightOpen = false;

            if (IsCellValid(current.westNeighbour))
            {
                adjacents.Add(current.westNeighbour);
                leftOpen = true;
            }
            if (IsCellValid(current.eastNeighbour))
            {
                adjacents.Add(current.eastNeighbour);
                rightOpen = true;
            }
            if (IsCellValid(current.northNeighbour))
            {
                if (allowDiagonals)
                {
                    AddAdjacentRow(current.northNeighbour, adjacents, leftOpen, rightOpen);
                }
                else
                {
                    adjacents.Add(current.northNeighbour);
                }
            }
            if (IsCellValid(current.southNeighbour))
            {
                if (allowDiagonals)
                {
                    AddAdjacentRow(current.southNeighbour, adjacents, leftOpen, rightOpen); 
                }
                else
                {
                    adjacents.Add(current.southNeighbour);
                }
            }

            return adjacents;
        }

        private void AddAdjacentRow(GridCellWithNeighbours mid, List<GridCellWithNeighbours> adjacents, bool leftOpen, bool rightOpen)
        {
            adjacents.Add(mid);
            if (leftOpen && IsCellValid(mid.westNeighbour))
            {
                adjacents.Add(mid.westNeighbour);
            }
            if (rightOpen && IsCellValid(mid.eastNeighbour))
            {
                adjacents.Add(mid.eastNeighbour);
            }
        }

        protected virtual bool IsCellValid(GridCell adjacent)
        {
            return adjacent != null && adjacent.IsFree();
        }

        private List<GridCell> BacktrackPath(Dictionary<GridCell, GridCell> backtrackingList, GridCell end)
        {
            List<GridCell> result = new List<GridCell>();

            GridCell current = end;
            while (current != null && backtrackingList.ContainsKey(current))
            {
                result.Insert(0, current);
                current = backtrackingList[current];
            }

            return result;
        }

        private float h(GridCell cell)
        {
            return (center - cell.transform.position).magnitude;
        }
    } 
}
