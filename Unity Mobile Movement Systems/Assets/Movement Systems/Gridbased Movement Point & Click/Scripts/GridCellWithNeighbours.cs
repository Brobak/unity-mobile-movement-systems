﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public class GridCellWithNeighbours : GridCell
    {
        [SerializeField]
        private GridCellWithNeighbours[] neighbours = new GridCellWithNeighbours[4];

        public IEnumerable<GridCellWithNeighbours> adjacents()
        {
            foreach (GridCellWithNeighbours neighbour in neighbours)
            {
                yield return neighbour;
            }
        }

        public GridCellWithNeighbours northNeighbour
        {
            get { return neighbours[0]; }
            set { neighbours[0] = value; }
        }
        public GridCellWithNeighbours eastNeighbour
        {
            get { return neighbours[1]; }
            set { neighbours[1] = value; }
        }
        public GridCellWithNeighbours southNeighbour
        {
            get { return neighbours[2]; }
            set { neighbours[2] = value; }
        }
        public GridCellWithNeighbours westNeighbour
        {
            get { return neighbours[3]; }
            set { neighbours[3] = value; }
        }
    }
}
