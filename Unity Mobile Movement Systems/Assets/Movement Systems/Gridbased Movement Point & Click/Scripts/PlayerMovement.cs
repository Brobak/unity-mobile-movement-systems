using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public class PlayerMovement : GridWalker
    {
        private IClickableObject currentTarget;

        public void PathToTarget(IClickableObject target)
        {
            SetNewPath(target.interactionTargetCells.ToArray());

            if (currentPath == null)
            {
                return;
            }

            currentTarget = target;

            StartPath();
        }

        public override void PathToCell(GridCell newCell)
        {
            currentTarget = null;

            base.PathToCell(newCell);
        }

        protected override void FollowPath()
        {
            if (!currentPath.Any() && currentTarget != null)
            {
                Vector3 lookDir = currentTarget.transform.position - transform.position;
                if (lookDir != Vector3.zero)
                {
                    transform.forward = lookDir; 
                }
                InteractWithTarget();
            }

            base.FollowPath();
        }

        protected override void RecalculatePath()
        {
            if (currentTarget != null)
            {
                PathToTarget(currentTarget);
            }
            base.RecalculatePath();
        }

        private void InteractWithTarget()
        {
            Interactable interactable = currentTarget as Interactable;
            currentTarget = null;
            if (interactable != null)
            {
                interactable.Interact(this);
            }
        }
    }
}