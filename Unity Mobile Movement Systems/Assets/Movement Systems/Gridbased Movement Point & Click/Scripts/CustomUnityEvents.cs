﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GridbasedMovementPointNClick
{
    [Serializable]
    public class GridCellUnityEvent : UnityEvent<GridCell>
    {
        
    }

    [Serializable]
    public class ClickableTargetUnityEvent : UnityEvent<IClickableObject>
    {

    }
}
