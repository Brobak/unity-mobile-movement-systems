﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public abstract class Interactable : ObjectInGrid, IClickableObject
    {
        public virtual IEnumerable<GridCell> interactionTargetCells =>
            ((GridCellWithNeighbours) currentCell).adjacents().Where(c => c != null);

        public abstract void Interact(Moveable moveable);
    } 
}
