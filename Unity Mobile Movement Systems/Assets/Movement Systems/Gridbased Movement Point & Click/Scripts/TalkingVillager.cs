﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GridbasedMovementPointNClick
{
    public class TalkingVillager : Interactable
    {
        public Text fieldForTalk;
        public string dialogue = "Hello";

        private bool isTalking = false;

        public override IEnumerable<GridCell> interactionTargetCells => new []
        {
            Grid.grid.FindCell(transform.position + transform.forward)
        };

        public override void Interact(Moveable moveable)
        {
            if (isTalking)
            {
                fieldForTalk.text = "";
                isTalking = false;
            }
            else
            {
                fieldForTalk.text = dialogue;
                isTalking = true;
            }
        }
    } 
}
