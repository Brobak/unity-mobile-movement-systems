﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public interface IClickableObject
    {
        Transform transform { get; }

        GameObject gameObject { get; }

        IEnumerable<GridCell> interactionTargetCells { get; }

        T GetComponent<T>();
    } 
}
