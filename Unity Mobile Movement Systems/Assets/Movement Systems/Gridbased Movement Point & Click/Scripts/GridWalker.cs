﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public abstract class GridWalker : Moveable
    {
        [SerializeField]
        protected bool allowDiagonalMovement = true;

        protected List<GridCell> currentPath;
        protected GridCell lastCell;

        public virtual void PathToCell(GridCell newCell)
        {
            SetNewPath(newCell);

            if (currentPath == null || !currentPath.Any())
            {
                return;
            }

            StartPath(); 
        }

        protected virtual void SetNewPath(params GridCell[] newCells)
        {
            AStarPathFinder pathFinder = new AStarPathFinder(currentCell, newCells, allowDiagonalMovement);
            currentPath = pathFinder.FindPath();

            if (currentPath == null)
            {
                Debug.LogWarning("No path found to " + newCells.First().transform.position);
                return;
            }
        }

        protected virtual void StartPath()
        {
            if (!isMoving || currentPath.First() == lastCell)
            {
                FollowPath();
            }
        }

        protected virtual void FollowPath()
        {
            if (currentPath.Any())
            {
                GridCell next = currentPath.First();
                if (next.IsFree())
                {
                    MoveInGrid(next);
                }
                else
                {
                   RecalculatePath();
                }
            }
        }

        protected virtual void RecalculatePath()
        {
            if (currentPath.Count > 1)
            {
                PathToCell(currentPath.Last());
            }
        }

        public override void MoveInGrid(GridCell _newCell)
        {
            currentPath.Remove(_newCell);

            base.MoveInGrid(_newCell);
        }

        protected override void ArriveAtCell(GridCell cell)
        {
            base.ArriveAtCell(cell);

            FollowPath();
        }

        public override void FreeCurrentCell()
        {
            lastCell = currentCell;

            base.FreeCurrentCell();
        }
    } 
}
