﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GridbasedMovementPointNClick
{
    public class Doorway : Interactable
    {
        public Doorway targetDoor;

        [SerializeField]
        private GridCell[] currentCells = new GridCell[3];

        public override IEnumerable<GridCell> interactionTargetCells => currentCells;

        private Vector3 landingPosition => targetDoor.transform.position - targetDoor.transform.right;

        public override void Interact(Moveable behaviour)
        {
            behaviour.MoveToPosition(landingPosition);
            behaviour.MoveInGrid(targetDoor.currentCell);
        }

        public override void OccupyCell(GridCell _newCell)
        {
            currentCell = _newCell;

            GridCell secondCell = Grid.grid.FindCell(_newCell.transform.position + transform.forward);
            GridCell thirdCell = Grid.grid.FindCell(_newCell.transform.position - transform.forward);
            
            currentCells[0] = secondCell;
            currentCells[1] = _newCell;
            currentCells[2] = thirdCell;
        }

        public override void FreeCurrentCell()
        {
            currentCell = null;
            for (int i = 0; i < currentCells.Length; i++)
            {
                currentCells[i] = null;
            }
        }
    } 
}
