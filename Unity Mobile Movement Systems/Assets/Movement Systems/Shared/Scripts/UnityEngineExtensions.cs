﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityEngineExtensions
{
    #region Vector3
    public static Vector3 WithX(this Vector3 vector, float x)
    {
        return new Vector3(x, vector.y, vector.z);
    }

    public static Vector3 WithY(this Vector3 vector, float y)
    {
        return new Vector3(vector.x, y, vector.z);
    }

    public static Vector3 WithZ(this Vector3 vector, float z)
    {
        return new Vector3(vector.x, vector.y, z);
    }
    #endregion

    #region Transform
    public static void SetPositionX(this Transform transform, float x)
    {
        transform.position = transform.position.WithX(x);
    }

    public static void SetPositionY(this Transform transform, float y)
    {
        transform.position = transform.position.WithX(y);
    }

    public static void SetPositionZ(this Transform transform, float z)
    {
        transform.position = transform.position.WithZ(z);
    }
    #endregion

    #region LayerMask
    public static bool ContainsLayer(this LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
    #endregion
}
