﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public Transform followTarget;

    private Vector3 vectorToTarget;

    private void Start()
    {
        vectorToTarget = followTarget.position - transform.position;
    }

    private void Update()
    {
        transform.position = followTarget.position - vectorToTarget;
    }
}
