﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public float tiltFactor = 0.5f;
    public bool automaticIdle = false;

    private bool isRunning = false;

    private void LateUpdate()
    {
        if(automaticIdle)
        {
            if(!isRunning)
            {
                Idle();
            }
            isRunning = false;
        }
    }

    public void Run(float relativeSpeed)
    {
        transform.localEulerAngles = new Vector3(tiltFactor * Mathf.Pow(relativeSpeed, 2), 0, 0);
        isRunning = true;
    }

    public void Idle()
    {
        transform.localEulerAngles = Vector3.zero;
    }
}
