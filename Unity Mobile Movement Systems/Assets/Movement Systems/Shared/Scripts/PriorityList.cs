﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PriorityList<T> : IReadOnlyList<T>
{
    private readonly List<T> values;
    private readonly List<float> priorities;

    public int Count => values.Count;
    
    public PriorityList()
    {
        values = new List<T>();
        priorities = new List<float>();
    }

    public PriorityList(List<T> values, List<float> priorities)
    {
        this.values = values.ToList();
        this.priorities = priorities.ToList();
    }

    public T this[int index] => values[index];

    public void Add(T item, float priority)
    {
        int i = 0;
        for (; i < Count; i++)
        {
            if (priorities[i] > priority)
            {
                break;
            }
        }
        values.Insert(i, item);
        priorities.Insert(i, priority);
    }

    public void UpdatePriority(T item, float priority)
    {
        Remove(item);
        Add(item, priority);
    }

    public float GetPriority(T item)
    {
        int index = values.IndexOf(item);
        if (index < 0)
        {
            throw new ArgumentOutOfRangeException();
        }

        return priorities[index];
    }

    public T Pop()
    {
        T value = values[0];
        RemoveAt(0);
        return value;
    }

    public void Clear()
    {
        values.Clear();
        priorities.Clear();
    }

    public bool Remove(T item)
    {
        int index = values.IndexOf(item);
        if (index < 0)
        {
            return false;
        }

        values.RemoveAt(index);
        priorities.RemoveAt(index);
        return true;
    }

    public void RemoveAt(int index)
    {
        values.RemoveAt(index);
        priorities.RemoveAt(index);
    }

    public bool Contains(T item) => values.Contains(item);

    public int IndexOf(T item) => values.IndexOf(item);

    public IEnumerator<T> GetEnumerator()
    {
        return values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
