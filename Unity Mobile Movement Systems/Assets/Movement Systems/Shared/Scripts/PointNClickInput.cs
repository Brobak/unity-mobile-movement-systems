﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PointNClickInput<T> : MonoBehaviour where T : class
{
    public LayerMask walkableArea;
        public LayerMask clickableObjects;

        [SerializeField]
        private bool holdToKeepWalking = true;
        [SerializeField]
        private float clickRepeatTimer = 0.5f;

        [SerializeField]
        private Camera pointerOriginCamera;

        private float touchStarted;

        private void Update()
        {
#if UNITY_ANDROID || UNITY_IOS
            HandleTouch();
#else
            HandleEditorClick();
#endif
        }

        private void HandleTouch()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    touchStarted = Time.time;

                    HandleScreenClick(touch.position);
                }
                else if (holdToKeepWalking && TouchHold(touch) && TimeToClick())
                {
                    touchStarted = Time.time;

                    HandleScreenClick(touch.position);
                }
            }
        }

        private static bool TouchHold(Touch touch)
        {
            return (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary);
        }

        private bool TimeToClick()
        {
            return Time.time - touchStarted > clickRepeatTimer;
        }

        private void HandleEditorClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                touchStarted = Time.time;

                HandleScreenClick(Input.mousePosition);
            }
            else if (holdToKeepWalking && Input.GetMouseButton(0) && TimeToClick())
            {
                touchStarted = Time.time;

                HandleScreenClick(Input.mousePosition);
            }
        }

        private void HandleScreenClick(Vector2 position)
        {
            Ray ray = pointerOriginCamera.ScreenPointToRay(position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, walkableArea | clickableObjects))
            {
                HandleHit(hit);
            }
        }

        private void HandleHit(RaycastHit hit)
        {
            int hitLayer = hit.collider.gameObject.layer;
            if (walkableArea.ContainsLayer(hitLayer))
            {
                OnMoveClick(hit);
            }
            else if (clickableObjects.ContainsLayer(hitLayer))
            {
                HandleObjectClick(hit.transform.gameObject);
            }
        }

        private void HandleObjectClick(GameObject hitObject)
        {
            T target = hitObject.GetComponent<T>();
            if (target == null)
            {
                Debug.LogError(hitObject.name + " is in clickable layer but is not a clickable object");
            }
            else
            {
                OnTargetClick(target);
            }
        }

        protected abstract void OnMoveClick(RaycastHit hit);

        protected abstract void OnTargetClick(T target);
}
